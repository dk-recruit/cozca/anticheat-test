package fr.arthursrv.darkoniaac.manager;

import java.util.ArrayList;
import java.util.List;

import fr.arthursrv.darkoniaac.cheat.AntiFall;
import fr.arthursrv.darkoniaac.cheat.Fly;
import fr.arthursrv.darkoniaac.cheat.Glide;
import fr.arthursrv.darkoniaac.cheat.Speed;

public class ModuleManagers {
	
	public List<ModuleCheck> checks;
	public ModuleManagers() {
		checks = new ArrayList<>();
		init();
	}
	
	private void init() {
		addCheck(new Speed());
		addCheck(new AntiFall());
		addCheck(new Glide());
		addCheck(new Fly());
		
	}
	
	public void addCheck(ModuleCheck check) {
		checks.add(check);
	}
	
	public void removeCheck(ModuleCheck check) {
		checks.remove(check);
	}

}
