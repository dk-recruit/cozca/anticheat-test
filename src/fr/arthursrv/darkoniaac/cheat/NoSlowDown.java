package fr.arthursrv.darkoniaac.cheat;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import fr.arthursrv.darkoniaac.manager.CheckType;
import fr.arthursrv.darkoniaac.manager.ModuleCheck;

public class NoSlowDown extends ModuleCheck  {

	public NoSlowDown() {
		super("NoSlowDown", CheckType.MOVEMENT, true, true, 3);
		
	}
	
	static ArrayList <Player> bowspanned = new ArrayList<>();
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer(); 
		if(!bowspanned.contains(p)) {
			return;
		}
		
		if(!p.isOnGround()) {
			return;
		}
		
		Location from = e.getFrom();
		Location to = e.getTo();
		if(e.getFrom().getY() != e.getTo().getX()) {
			return;
		}
		
		Vector vec = to.toVector();
		double dist = vec.distance(from.toVector());
		if(dist > 0.15D) {
			e.setCancelled(true);
			flag(p, "Cheat " + CheckType.MOVEMENT);
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if(p.getItemInHand() != null) {
			if(p.getItemInHand().getType().equals(Material.BOW)) {
				if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() ==  Action.RIGHT_CLICK_BLOCK) {
					if(p.getInventory().contains(Material.ARROW)) {
						bowspanned.add(p);
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onBowShot(EntityShootBowEvent e) {
		if(e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			bowspanned.remove(p);
		}
	}
	
	@EventHandler
	public void onSwitch(PlayerItemHeldEvent e) {
		Player p = e.getPlayer();
		if(bowspanned.contains(p)) {
			bowspanned.remove(p);
		}
	}

}
