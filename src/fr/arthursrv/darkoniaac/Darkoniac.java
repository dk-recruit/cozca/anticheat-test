package fr.arthursrv.darkoniaac;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import fr.arthursrv.darkoniaac.manager.ModuleManagers;

public class Darkoniac extends JavaPlugin {
	
	public static Darkoniac instance;
	private ModuleManagers managers;
	
	@Override
	public void onEnable() {
		Bukkit.getConsoleSender().sendMessage("L'Anticheat est allume");
		
		instance = this;
		managers = new ModuleManagers();
		}
	@Override
	public void onDisable() {
		Bukkit.getConsoleSender().sendMessage("L'Anticheat est eteint");	

	}
	
	public static Darkoniac getInstance() {
		return instance;
	}
}
